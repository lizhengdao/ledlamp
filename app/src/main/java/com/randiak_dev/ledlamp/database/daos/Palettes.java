package com.randiak_dev.ledlamp.database.daos;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.randiak_dev.ledlamp.database.entities.Palette;

import java.util.List;

@Dao
public interface Palettes {
    @Query("Select * from Palette WHERE Preset = 1")
    public List<Palette> getPresetPalettesList();
    @Query("Select title from Palette WHERE Preset = 1")
    public List<String> getPresetPalettesTitles();
    @Query("Select * from Palette WHERE Preset = 0")
    public List<Palette> getCustomPalettesList();
    @Query("Select title from Palette WHERE Preset = 0")
    public List<String> getCustomPalettesTitles();
    @Query("Select * from Palette WHERE id = :id")
    public Palette getPalette(int id);
    @Insert
    void insertPalette(Palette palette);
    @Delete
    void deletePalette(Palette palette);
}
