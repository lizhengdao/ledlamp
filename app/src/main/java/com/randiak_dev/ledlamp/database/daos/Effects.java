package com.randiak_dev.ledlamp.database.daos;

import androidx.room.Dao;
import androidx.room.Query;

import com.randiak_dev.ledlamp.database.entities.Effect;
import com.randiak_dev.ledlamp.database.entities.Attribute;

import java.util.List;

@Dao
public interface Effects {
    @Query("Select * from Effect")
    List<Effect> getEffectsList();
    @Query("Select * from Effect WHERE id = :id")
    Effect getEffect(int id);
    @Query("Select title from Effect")
    List<String> getEffectsTitlesList();

    @Query("Select * from Attribute WHERE name = :attributeName")
    Attribute getEffectAttributes(String attributeName);
}
