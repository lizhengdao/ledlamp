package com.randiak_dev.ledlamp.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.randiak_dev.ledlamp.database.daos.Effects;
import com.randiak_dev.ledlamp.database.daos.Palettes;
import com.randiak_dev.ledlamp.database.entities.Attribute;
import com.randiak_dev.ledlamp.database.entities.Effect;
import com.randiak_dev.ledlamp.database.entities.Palette;

@Database(entities = {Palette.class, Effect.class, Attribute.class}, exportSchema = false, version = 1)
@TypeConverters({Converters.class})
public abstract class LampDatabase extends RoomDatabase {
    private static final String DB_NAME = "database.db";
    private static LampDatabase instance;

    public static synchronized LampDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(), LampDatabase.class, DB_NAME)
                    .allowMainThreadQueries()
//                    .fallbackToDestructiveMigration()
                    .createFromAsset(DB_NAME)
                    .build(); // .fallbackToDestructiveMigration()
        }
        return instance;
    }

    public abstract Palettes palettes();
    public abstract Effects effects();
}
