package com.randiak_dev.ledlamp.database.entities;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.List;

@Entity
public class Effect {
    @PrimaryKey
    private int id;
    @NonNull
    private int command;
    @NonNull
    private String title;
    private List<String> attributesNames;

    public Effect(int id, int command, String title, List<String> attributesNames) {
        this.id = id;
        this.command = command;
        this.title = title;
        this.attributesNames = attributesNames;
    }

    public int getId() {
        return id;
    }

    public int getCommand() {
        return command;
    }

    public String getTitle() {
        return title;
    }

    public List<String> getAttributesNames() {
        return attributesNames;
    }
}
