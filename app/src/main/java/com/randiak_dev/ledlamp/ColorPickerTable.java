package com.randiak_dev.ledlamp;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;

import com.randiak_dev.ledlamp.ui.ColorButton;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ColorPickerTable extends TableLayout {
    private int buttonsMargin;
    private int maxButtonSize;
    private int buttonsGravity;

    private static Context context;
    LinkedList<TableRow> tableRows;

    public ColorPickerTable(Context context) {
        super(context);
        this.context = context;
        this.tableRows = null;
    }

    public ColorPickerTable(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.ColorPickerTable, 0, 0);
        try {
            this.buttonsMargin = (int) (typedArray.getDimension(R.styleable.ColorPickerTable_buttonsMargin, 0) / 2);
            this.maxButtonSize = (int) typedArray.getDimension(R.styleable.ColorPickerTable_maxButtonsSize, 100);
            int gravity = typedArray.getInteger(R.styleable.ColorPickerTable_buttonsGravity, 0);
            if (gravity == 0) {
                this.buttonsGravity = Gravity.LEFT;
            } else if (gravity == 1) {
                this.buttonsGravity = Gravity.CENTER;
            } else if (gravity == 2) {
                this.buttonsGravity = Gravity.RIGHT;
            }
        } finally {
            typedArray.recycle();
        }

        this.context = context;
        this.tableRows = null;
    }

    public void createTable(int nColors) {
        if (super.getWidth() > 0) {
            int tableWidth = View.MeasureSpec.getSize(super.getWidth());
            int itemsPerRow = tableWidth / (maxButtonSize + buttonsMargin * 2);
            int itemSize = tableWidth / itemsPerRow - buttonsMargin * 2;
            if (itemsPerRow > 0) {
                this.clearTable();
                this.tableRows = new LinkedList<>();
                for (int i = 0; i < nColors; i++) {
                    if (i % itemsPerRow == 0) {
                        TableRow tableRow = new TableRow(this.context);
                        tableRow.setGravity(this.buttonsGravity);
                        super.addView(tableRow);
                        tableRows.add(tableRow);
                    }
                    ColorButton colorButton = new ColorButton(this.context);
                    TableRow.LayoutParams btLayoutParams = new TableRow.LayoutParams(itemSize, ViewGroup.LayoutParams.WRAP_CONTENT);
                    btLayoutParams.setMargins(buttonsMargin, buttonsMargin, buttonsMargin, buttonsMargin);
                    colorButton.setLayoutParams(btLayoutParams);
                    tableRows.getLast().addView(colorButton);
                }
            }
        }
    }

    public void clearTable() {
        if (this.tableRows != null) {
            this.tableRows.clear();
            this.tableRows = null;
            super.removeAllViews();
        }
    }

    public List<Integer> getColors() {
        ArrayList<Integer> colors = new ArrayList<>();
        if (tableRows == null || tableRows.size() == 0) {
            return colors;
        }
        for (TableRow tableRow : tableRows) {
            for (int i = 0; i < tableRow.getChildCount(); i++) {
                colors.add(((ColorButton)tableRow.getChildAt(i)).getColor());
            }
        }
        return colors;
    }
}
