package com.randiak_dev.ledlamp;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.view.Menu;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.UUID;
import java.util.concurrent.Semaphore;

public class BluetoothHandler extends Activity {
    private Context context;
    private static final String TAG = "BT_HANDLER_TAG";

    private BluetoothAdapter bluetoothAdapter;
    private BluetoothSocket bluetoothSocket;
    private UUID uuid;

    private BluetoothThread connectedThread;

    public BluetoothHandler(Context pContext) {
        this.context = pContext;
        this.bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        this.uuid = UUID.randomUUID();
        this.connectedThread = BluetoothThread.getInstance();
    }

    public void enableBT() {
        if (this.bluetoothAdapter != null && !this.bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            context.startActivity(enableBtIntent);
        }
    }

    public void disableBT() {
        this.disconnect();
        this.bluetoothAdapter.disable();
    }

    public void setVisible(boolean visible){
        Intent setVisibleIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        context.startActivity(setVisibleIntent);
    }

    public void setUuid(byte[] pUuuid) {
        this.uuid = UUID.nameUUIDFromBytes(pUuuid);
    }

    public ArrayList<BluetoothDevice> getPairedDevices(){
        return new ArrayList<BluetoothDevice>(this.bluetoothAdapter.getBondedDevices());
    }

    public String getAddressOf(int pIndex) {
        return getPairedDevices().get(pIndex).getAddress();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        ArrayList<BluetoothDevice> pairedDevices = getPairedDevices();
        for (int i = 0; i < pairedDevices.size(); i++) {
            menu.add(0, i, Menu.NONE, pairedDevices.get(i).getName());
        }
        return super.onPrepareOptionsMenu(menu);
    }

    public boolean connect(String pAddress) {
        BluetoothDevice device = this.bluetoothAdapter.getRemoteDevice(pAddress);
        return this.connectedThread.connect(device);
    }

    public void disconnect() {
        this.connectedThread.disconnect();
    }
}
