package com.randiak_dev.ledlamp.ui.main.Patterns;

import android.os.Bundle;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.randiak_dev.ledlamp.MainActivity;
import com.randiak_dev.ledlamp.R;

public class PatternFragment extends Fragment {
    private View view;

    private byte command = 0x00;
    private boolean[] parameters = {false, false, false, false, false};
    private String[] parNames;
    private byte[] minValues = {0, 0, 0, 0};
    private byte[] maxValues = {0, 0, 0, 0};
    private byte[] deltas = {0, 0, 0, 0};

    private LinearLayout[] layouts;
    private TextView[] textViews;
    private SeekBar[] seekBars;
    private Switch aSwitch;
    private Button button;

    private byte[] values;
    private boolean clockwise;

    public PatternFragment() {
        layouts = new LinearLayout[5];
        textViews = new TextView[4];
        seekBars = new SeekBar[4];
        values = new byte[4];
        parNames = new String[4];
    }

    public void setUp(byte pCommand, boolean[] pParameters, String[] pParNames,
                           byte[] pMinValues, byte[] pMaxValues, byte[] pDeltas, byte[] pDefaults) {
        command = pCommand;
        parameters = pParameters;
        parNames = pParNames;
        minValues = pMinValues;
        maxValues = pMaxValues;
        deltas = pDeltas;
        values = pDefaults;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.ultimate_pattern_fm, container, false);

        layouts[0] = view.findViewById(R.id.patternLayout1);
        layouts[1] = view.findViewById(R.id.patternLayout2);
        layouts[2] = view.findViewById(R.id.patternLayout3);
        layouts[3] = view.findViewById(R.id.patternLayout4);
        layouts[4] = view.findViewById(R.id.patternLayout5);

        textViews[0] = view.findViewById(R.id.tvPattern1);
        textViews[1] = view.findViewById(R.id.tvPattern2);
        textViews[2] = view.findViewById(R.id.tvPattern3);
        textViews[3] = view.findViewById(R.id.tvPattern4);

        seekBars[0] = view.findViewById(R.id.sbPattern1);
        seekBars[1] = view.findViewById(R.id.sbPattern2);
        seekBars[2] = view.findViewById(R.id.sbPattern3);
        seekBars[3] = view.findViewById(R.id.sbPattern4);

        for (int i = 0; i < seekBars.length; i++) {
//            seekBars[i].setMin((int)minValues[i]);
            seekBars[i].setMax((int)(maxValues[i] & 0xFF));
            seekBars[i].setAlpha((int)deltas[i]);
            seekBars[i].setProgress((int)(values[i] & 0xFF));
        }

        aSwitch = view.findViewById(R.id.swPattern);

        for (int i = 0; i < layouts.length; i++) {
            if (parameters[i])
                layouts[i].setVisibility(View.VISIBLE);
            else
                layouts[i].setVisibility(View.INVISIBLE);
        }

        button = view.findViewById(R.id.btPatternOK);

        for (int i = 0; i < seekBars.length; i++) {
            final int index = i;
            seekBars[i].setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    if (parameters[index]) {
                        values[index] = (byte)progress;
                        valuesChanged();
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) { }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) { }
            });
        }

        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                clockwise = isChecked;
                valuesChanged();
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendData();
            }
        });

        valuesChanged();

        return view;
    }

    private void valuesChanged() {
        for (int i = 0; i < parNames.length; i++) {
            textViews[i].setText(parNames[i] + " (" + String.valueOf(values[i] & 0xFF) + ")");
        }
    }

    private void sendData() {
        if (parameters[4])
            values[3] = clockwise ? (byte)1 : (byte)0;
//        ((MainActivity)getActivity()).dataHandler.(command, values);
    }
}
