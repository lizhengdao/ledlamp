package com.randiak_dev.ledlamp.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import androidx.annotation.NonNull;

import com.randiak_dev.ledlamp.R;
import com.rtugeek.android.colorseekbar.ColorSeekBar;

public class ColorPickerDialog extends Dialog implements View.OnClickListener {
    private ColorSeekBar sbPickColor;
    private Button btCancel, btPick;
    private OnColorPickedListener onColorPickedListener;

    public ColorPickerDialog(@NonNull Context context) {
        super(context);
        onColorPickedListener = null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.color_picker_dialog_fm);
        sbPickColor = findViewById(R.id.sbPickColor);
        btCancel = findViewById(R.id.btCancel);
        btPick = findViewById(R.id.btPick);
        btCancel.setOnClickListener(this);
        btPick.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == btPick.getId()) {
            if (onColorPickedListener != null) {
                onColorPickedListener.onColorPicked(sbPickColor.getColor());
            }
        }
        dismiss();
    }

    public interface OnColorPickedListener {
        public void onColorPicked(int color);
    }

    public void setOnColorPickedListener(OnColorPickedListener onColorPickedListener) {
        this.onColorPickedListener = onColorPickedListener;
    }
}
