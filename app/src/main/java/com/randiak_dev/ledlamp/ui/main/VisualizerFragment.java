package com.randiak_dev.ledlamp.ui.main;

import android.content.res.Resources;
import android.media.audiofx.Visualizer;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.randiak_dev.ledlamp.DataHandler;
import com.randiak_dev.ledlamp.MainActivity;
import com.randiak_dev.ledlamp.R;
import com.randiak_dev.ledlamp.visualizer.BarVisualizerView;
import com.randiak_dev.ledlamp.visualizer.VisualizerThread;
import com.randiak_dev.r2itseekbar.R2ITSeekBar;

public class VisualizerFragment extends Fragment {
    private View view;
    private DataHandler dataHandler;

    private Switch swVisualize;

    private Spinner spMode;
    private Spinner spBars;
    private int[] barsValues;
    private R2ITSeekBar sbSpace;
    private R2ITSeekBar sbWidth;

    private Spinner spColorMode;
    private R2ITSeekBar sbColorStep;

    private Spinner spBrtMode;
    private R2ITSeekBar sbBrightnessStep;

    private int mode = 0;
    private int bars = 16;
    private int width = 5;
    private int space = 1;

    private int colorMode = 0;
    private int colorStep = 3;

    private int brtMode = 0;
    private int brightnessStep = 254;

    boolean visualize = false;

    private static final int CAPTURE_SIZE = 1024;
    Visualizer.MeasurementPeakRms mPeakRms;

    private VisualizerThread thread;

    public VisualizerFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.visualizer_fragment, container, false);

        swVisualize = view.findViewById(R.id.swVisualize);

        spMode = view.findViewById(R.id.spVisMode);
        spBars = view.findViewById(R.id.spBars);
        sbSpace = view.findViewById(R.id.sbSpace);
        sbWidth = view.findViewById(R.id.sbWidth);

        spColorMode = view.findViewById(R.id.spClrMode);
        sbColorStep = view.findViewById(R.id.sbColorStep);

        spBrtMode = view.findViewById(R.id.spBrtMode);
        sbBrightnessStep = view.findViewById(R.id.sbBrightnessStep);

        if (thread == null) {
            thread = (((MainActivity) getActivity()).visualizerThread);
        }

        Resources resources = getResources();
        barsValues = resources.getIntArray(R.array.inBars);

        swVisualize.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    thread.start();
                    processParameters0();
                    processParameters1();
                    processParameters2();
                } else {
                    thread.interrupt();
                }
                visualize = isChecked;
            }
        });

        spMode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mode = position;
                processParameters0();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spBars.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                bars = barsValues[position];
                thread.setBars(bars);
                processParameters0();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spBars.setSelection(6);

        sbWidth.setCustomSeekBarListener(new R2ITSeekBar.CustomSeekBarListener() {
            @Override
            public void onProgressChanged(int progress) {
                width = progress;
                processParameters0();
            }
        });

        sbSpace.setCustomSeekBarListener(new R2ITSeekBar.CustomSeekBarListener() {
            @Override
            public void onProgressChanged(int progress) {
                space = progress;
                processParameters0();
            }
        });

        spColorMode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                colorMode = position;
                processParameters1();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sbColorStep.setCustomSeekBarListener(new R2ITSeekBar.CustomSeekBarListener() {
            @Override
            public void onProgressChanged(int progress) {
                colorStep = progress;
                processParameters1();
            }
        });

        spBrtMode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                brtMode = position;
                processParameters2();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sbBrightnessStep.setCustomSeekBarListener(new R2ITSeekBar.CustomSeekBarListener() {
            @Override
            public void onProgressChanged(int progress) {
                brightnessStep = progress;
                processParameters2();
            }
        });

        BarVisualizerView barVisualizer = view.findViewById(R.id.barVisualizer);

        dataHandler = DataHandler.getInstance();
        dataHandler.setBarVisualizer(barVisualizer);

        return view;
    }

    private void processParameters0() {
        if (visualize) {
            this.dataHandler.sendVisualizePars0(this.mode, this.bars, this.width, this.space);
        }
    }

    private void processParameters1() {
        if (visualize) {
            this.dataHandler.sendVisualizePars1(this.colorMode, this.colorStep);
        }
    }

    private void processParameters2() {
        if (visualize) {
            this.dataHandler.sendVisualizePars2(this.brtMode, this.brightnessStep);
        }
    }
}