package com.randiak_dev.ledlamp.ui;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.os.Parcelable;
import android.view.Gravity;
import android.view.View;

import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;

import com.randiak_dev.ledlamp.R;

import java.util.Random;

public class ColorButton extends androidx.appcompat.widget.AppCompatButton
        implements View.OnClickListener {
    private View background;
    private Context context;
    private int color;
    private static final double heightToWidth = 1.0;

    public ColorButton(Context context) {
        super(context);
        this.context = context;
        super.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        ColorPickerDialog colorPickerDialog = new ColorPickerDialog(context);
        colorPickerDialog.setOnColorPickedListener(new ColorPickerDialog.OnColorPickedListener() {
            @Override
            public void onColorPicked(int color) {
                setColor(color);
            }
        });
        colorPickerDialog.show();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        int size;
        if (widthMode == MeasureSpec.EXACTLY && widthSize > 0) {
            size = widthSize;
        } else if (heightMode == MeasureSpec.EXACTLY && heightSize > 0) {
            size = heightSize;
        } else {
            size = widthSize < heightSize ? widthSize : heightSize;
        }
        int finalMeasureSpec = MeasureSpec.makeMeasureSpec(size, MeasureSpec.EXACTLY);
        super.onMeasure(finalMeasureSpec, finalMeasureSpec);
    }

    public void setColor(int color) {
        this.color = color;
        int bgColor = 0xFF000000;
        if (!((color >> 3) > 0x3F || Color.luminance(color) > 0.5 || color == 0 || color == 255)) {
            bgColor = 0xFFFFFFFF;
        }
        super.setTextColor(bgColor);
        setBackground(color, bgColor);
    }

    private void setBackground(int color, int bgColor) {
        Drawable bottomLayer = ContextCompat.getDrawable(context, R.drawable.borders_black);
        if (bgColor == 0xFFFFFFFF) {
            bottomLayer = ContextCompat.getDrawable(context, R.drawable.borders_gray);
        }
        ColorDrawable topLayer = new ColorDrawable();
        topLayer.setColor(color);
        LayerDrawable buttonIcon = new LayerDrawable(new Drawable[]{ bottomLayer, topLayer});
        buttonIcon.setLayerSize(1, super.getHeight() - 20, super.getWidth() - 20);
        buttonIcon.setLayerGravity(1, Gravity.CENTER);
        super.setBackground(buttonIcon);
    }

    public int getColor() {
        return this.color;
    }
}
