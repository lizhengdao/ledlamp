package com.randiak_dev.ledlamp;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.view.View;
import android.widget.Toast;

import androidx.core.graphics.ColorUtils;

import com.randiak_dev.ledlamp.visualizer.BarVisualizerView;
import com.randiak_dev.ledlamp.visualizer.VisualizerThread;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class DataHandler extends Thread implements VisualizerThread.VisualizerEngineListener {
    private static final int COLOR = 0x10;
    private static final int SPECIAL_PALETTE = 0x11;
    private static final int BEACON_PALETTE = 0x12;
    private static final int CUSTOM_PALETTE_2 = 0x13;
    private static final int CUSTOM_PALETTE_3 = 0x14;
    private static final int CUSTOM_PALETTE_4 = 0x15;
    private static final int CUSTOM_PALETTE_16 = 0x16;

    private static final int DIRECTION = 0x20;
    private static final int BRIGHTNESS = 0x21;
    private static final int FADE_BY = 0x22;
    private static final int SPEED = 0x23;
    private static final int GLITTER = 0x24;
    private static final int STROBE = 0x25;

    private static final int MODE_MASK = 0x3F;

    private static final int VISUALIZE = 0x40;
    private static final int VISUALIZE_PARS_0 = 0x41;
    private static final int VISUALIZE_PARS_1 = 0x42;
    private static final int VISUALIZE_PARS_2 = 0x43;

    private static DataHandler instance;

    private BluetoothThread bluetoothThread;
    private VisualizerThread visualizerThread;
    private BarVisualizerView barVisualizer;

    private int actualSection = 0;
    private LinkedList<Section> sections;
    private Context context;

    public static synchronized DataHandler getInstance(BluetoothHandler bluetoothHandler,
                                                       VisualizerThread visualizerThread, Context context) {
        if (instance == null) {
            instance = new DataHandler(visualizerThread, context);
        }
        return instance;
    }

    public static synchronized DataHandler getInstance() {
        return instance;
    }

    private DataHandler(VisualizerThread visualizerThread, Context context) {
        this.bluetoothThread = BluetoothThread.getInstance();
        this.visualizerThread = visualizerThread;
        this.context = context;

        this.visualizerThread.setVisualizerEngineListener(this);

        sections = new LinkedList<>();
        sections.add(new Section());
    }

    public void setBarVisualizer(BarVisualizerView barVisualizer) {
        this.barVisualizer = barVisualizer;
    }

    @Override
    public void onFftDataProcessed(float[] samples) {
        if (barVisualizer != null) {
            barVisualizer.setFFT(samples);
        }
        this.sendVisualizeData(samples);
    }

    public void setColor(int color) {
        color = ColorUtils.compositeColors(color, Color.WHITE);
        this.sections.get(this.actualSection).color = color;
        this.sections.get(this.actualSection).colorMode = 0;
        int[] data = {COLOR, this.actualSection, Color.red(color), Color.green(color), Color.blue(color)};
        this.bluetoothThread.pushWriteData(this.processData(data));
    }

    public void setSpecialPalette(int paletteId) {
        this.sections.get(this.actualSection).specialPalette = paletteId;
        this.sections.get(this.actualSection).colorMode = 1;
        int[] data = {SPECIAL_PALETTE, this.actualSection, paletteId};
        this.bluetoothThread.pushWriteData(this.processData(data));
    }

    public void setBeaconPalette() {

    }

    public void setCustomPalette(int[] colors) {
        this.sections.get(this.actualSection).customPalette = colors;
        this.sections.get(this.actualSection).colorMode = 3;
        int command = 0;
        switch (colors.length) {
            case 2:
                command = CUSTOM_PALETTE_2;
                break;
            case 3:
                command = CUSTOM_PALETTE_3;
                break;
            case 4:
                command = CUSTOM_PALETTE_4;
                break;
            case 16:
                command = CUSTOM_PALETTE_16;
                break;
        }
        if (command != 0) {
            int frames = colors.length;
            byte[] sendData = new byte[frames * 8];
            for (int i = 0; i < frames; i++) {
                int[] data = {command, this.actualSection, i,
                        Color.red(colors[i]), Color.green(colors[i]), Color.blue(colors[i])};
                System.arraycopy(this.processData(data), 0, sendData, 8 * i, 8);
            }
            bluetoothThread.pushWriteData(sendData);
        }
    }

    public void setCustomPalette(int paletteId, int blending) {
    }

    public void setMode(int mode, int[] parameters) {
        this.sections.get(this.actualSection).mode = mode;
        int length = (parameters.length < this.sections.get(this.actualSection).MAX_PARS)
                ? parameters.length : this.sections.get(this.actualSection).MAX_PARS;
        for (int i = 0; i < length; i++) {
            this.sections.get(this.actualSection).parameters[i] = parameters[i];
        }
        int[] lPars = this.sections.get(this.actualSection).parameters;
        int[] data = {MODE_MASK & (mode | 0xF0), this.actualSection, lPars[0], lPars[1], lPars[2], lPars[3]};
        this.bluetoothThread.pushWriteData(this.processData(data));
    }

    public void setSpeed(int speed) {
        this.sections.get(this.actualSection).speed = speed;
        int[] data = {SPEED, this.actualSection, speed};
        this.bluetoothThread.pushWriteData(this.processData(data));
    }

    public void setBrightness(int brightness) {
        this.sections.get(this.actualSection).brightness = brightness;
        int[] data = {BRIGHTNESS, this.actualSection, brightness};
        this.bluetoothThread.pushWriteData(this.processData(data));
    }

    public void setFadeBy(int fadeBy) {
        this.sections.get(this.actualSection).fadeBy = fadeBy;
        int[] data = {FADE_BY, this.actualSection, fadeBy};
        this.bluetoothThread.pushWriteData(this.processData(data));
    }

    public void setDirection(int direction) {
        this.sections.get(this.actualSection).direction = direction;
        int[] data = {DIRECTION, this.actualSection, direction};
        this.bluetoothThread.pushWriteData(this.processData(data));
    }

    public void setGlitter(int glitter) {
        this.sections.get(this.actualSection).glitterRate = glitter;
        int[] data = {GLITTER, this.actualSection, glitter};
        this.bluetoothThread.pushWriteData(this.processData(data));
    }

    public void setStrobe(int strobe) {
        this.sections.get(this.actualSection).strobeRate = strobe;
        int[] data = {STROBE, this.actualSection, strobe};
        this.bluetoothThread.pushWriteData(this.processData(data));
    }

    public void sendVisualizePars0(int mode, int nBars, int width, int space) {
        int[] data = {VISUALIZE_PARS_0, mode, nBars, width, space};
        this.bluetoothThread.pushWriteData(this.processData(data));
    }

    public void sendVisualizePars1(int colorMode, int colorStep) {
        int[] data = {VISUALIZE_PARS_1, colorMode, colorStep};
        this.bluetoothThread.pushWriteData(this.processData(data));
    }

    public void sendVisualizePars2(int brightnessMode, int brightnessStep) {
        int[] data = {VISUALIZE_PARS_2, brightnessMode, brightnessStep};
        this.bluetoothThread.pushWriteData(this.processData(data));
    }

    public void sendVisualizeData(float[] samples) {
        if (bluetoothThread != null) {
            int frames = samples.length / 4;
            byte[] sendData = new byte[frames * 8];
            for (int i = 0; i < frames; i++) {
                int[] data = {VISUALIZE, i,
                        (int) (samples[4 * i] * 255), (int) (samples[4 * i + 1] * 255),
                        (int) (samples[4 * i + 2] * 255), (int) (samples[4 * i + 3] * 255)};
                System.arraycopy(this.processData(data), 0, sendData, 8 * i, 8);
            }
            bluetoothThread.pushWriteData(sendData);
        }
    }

    private void sendOneFrame(int[] data) {

    }

    private void sendMultipleFrames(int[] data) {

    }

    private byte[] processData(int[] data) {
        byte[] lData = new byte[8];
        Arrays.fill(lData, (byte)0);
        int length = data.length <= 6 ? data.length : 6;
        for (int i = 0; i < length; i++) {
            lData[i] = (byte) data[i];
        }
        int crc = crc16(lData);
        lData[6] = (byte)(crc >> 8);
        lData[7] = (byte)(crc & 0x00FF);
        return lData;
    }

    private int crc16(final byte[] pData) {
        int crc = 0x1D0F;
        for (int i = 0; i < (pData.length - 2); i++) {
            crc = ((crc >>> 8) | (crc << 8)) & 0xFFFF;
            crc ^= (pData[i] & 0xFF);
            crc ^= (crc & 0xFF) >> 4;
            crc ^= (crc << 12) & 0xFFFF;
            crc ^= ((crc & 0xFF) << 5) & 0xFFFF;
        }
        crc &= 0xFFFF;
        return crc;
    }

    Toast toast;
    private void showToast(final Context context, final String message) {
        if (toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        toast.show();
    }

    private class Section {
        public int speed;
        public int brightness;
        public int fadeBy;
        public int direction;

        public int color;
        public int specialPalette;
        public int[] customPalette;
        public int colorMode;

        public int mode;
        public static final int MAX_PARS = 4;
        public int[] parameters;

        public int glitterRate;
        public int strobeRate;

        public Section() {
            parameters = new int[MAX_PARS];
        }
    }
}
