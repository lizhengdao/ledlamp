package com.randiak_dev.ledlamp.visualizer;

public class Weightings {
    public enum Weighting {A, B, C, NONE}

    public static double[] calcWeightings(Weighting weightingType, int n, double minF, double maxF) {
        double[] weighting = new double[n];
        if (weightingType != Weighting.NONE) {
            for (int i = 0; i < n; i++) {
                double freq = (maxF - minF) / n * i + minF;
                if (weightingType == Weighting.A) {
                    weighting[i] = AWeighting(freq);
                } else if (weightingType == Weighting.B) {
                    weighting[i] = BWeighting(freq);
                } else if (weightingType == Weighting.C) {
                    weighting[i] = CWeighting(freq);
                }
            }
        }
        return weighting;
    }

    private static double AWeighting(double freq) {
        double freq2 = Math.pow(freq, 2);
        return 20 * Math.log10((Math.pow(12194, 2) * Math.pow(freq, 4)) /
                ((freq2 + Math.pow(20.6, 2)) * (freq2 + Math.pow(12194, 2)) *
                        Math.sqrt((freq2 + Math.pow(107.7, 2)) * (freq2 + Math.pow(737.9, 2))))) + 2;
    }

    private static double BWeighting(double freq) {
        double freq2 = Math.pow(freq, 2);
        return 20 * Math.log10((Math.pow(12194, 2) * Math.pow(freq, 3)) /
                ((freq2 + Math.pow(20.6, 2)) * (freq2 + Math.pow(12194, 2)) *
                        Math.sqrt(freq2 + Math.pow(158.5, 2)))) + 0.17;
    }

    private static double CWeighting(double freq) {
        double freq2 = Math.pow(freq, 2);
        return 20 * Math.log10((Math.pow(12194, 2) * Math.pow(freq, 2)) /
                ((freq2 + Math.pow(20.6, 2)) * (freq2 + Math.pow(12194, 2)))) + 0.06;
    }
}
