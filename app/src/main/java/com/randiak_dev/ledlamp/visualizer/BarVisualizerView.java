package com.randiak_dev.ledlamp.visualizer;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class BarVisualizerView extends View {
    private final int paintColor = Color.LTGRAY;
    private Paint drawPaint;

    private boolean mTop = false;
    private float[] mFFTPoints;
    private double spacing = 0.5;

    public BarVisualizerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFocusable(true);
        setFocusableInTouchMode(true);
        setupPaint();
    }

    private void setupPaint() {
        drawPaint = new Paint();
        drawPaint.setColor(paintColor);
        drawPaint.setAntiAlias(true);
        drawPaint.setStrokeWidth(1);
        drawPaint.setStyle(Paint.Style.STROKE);
        drawPaint.setStrokeJoin(Paint.Join.ROUND);
        drawPaint.setStrokeCap(Paint.Cap.ROUND);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (mFFTPoints != null) {
            canvas.drawLines(mFFTPoints, drawPaint);
        }
    }

    public void setFFT(float[] data)
    {
        int width = (int) (super.getWidth() * (1. - spacing / 2));
        int height = super.getHeight();
        float strokeWidth = (float) ((double) width / (data.length * (1. + spacing / 2)));
        drawPaint.setStrokeWidth(strokeWidth);
        mFFTPoints = new float[data.length * 4];
        for (int i = 0; i < data.length; i++) {
            mFFTPoints[i * 4 + 2] = (float) (i * ((float) width / data.length * (1. + spacing / 2)) + strokeWidth / 2);
            mFFTPoints[i * 4 + 0] = (float) (i * ((float) width / data.length * (1. + spacing / 2)) + strokeWidth / 2);

            if(mTop) {
                mFFTPoints[i * 4 + 1] = 0;
                mFFTPoints[i * 4 + 3] = data[i] * height;
            } else {
                mFFTPoints[i * 4 + 1] = height;
                mFFTPoints[i * 4 + 3] = height - data[i] * height;
            }
        }
        invalidate();
    }
}