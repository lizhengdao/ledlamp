package com.randiak_dev.ledlamp;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.UUID;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class BluetoothThread extends Thread {
    private static final String TAG = "BT_THREAD_TAG";
    private static BluetoothThread instance;
    private BluetoothSocket socket;
    private InputStream inputStream;
    private OutputStream outputStream;

    private static final int MAX_WRITE = 1024;
    private volatile byte[] writeData;
    private volatile int writeIndex = 0;
    private volatile int writeSize = 0;
    private Semaphore writeSem;
    private volatile LinkedList<Byte> readData;

    public static synchronized BluetoothThread getInstance() {
        if (instance == null) {
            instance = new BluetoothThread();
        }
        return instance;
    }

    private BluetoothThread() {
        writeData = new byte[MAX_WRITE];
        writeSem = new Semaphore(1);
        readData = new LinkedList<>();
    }

    public boolean connect(BluetoothDevice device) {
        if (this.socket != null && this.socket.isConnected()) {
            disconnect();
            this.socket = null;
        }
        UUID uuid = device.getUuids()[0].getUuid();
        try {
            this.socket = device.createRfcommSocketToServiceRecord(uuid);
//            this.bluetoothSocket = device.createInsecureRfcommSocketToServiceRecord(this.uuid);
            this.socket.connect();
            if (this.socket.isConnected()) {
                this.onConnect();
                return true;
            }
        } catch(IOException e) {
            Log.e(TAG, "Error connecting to socket", e);
        }
        return false;
    }

    private void onConnect() {
        InputStream tmpInput = null;
        try {
            tmpInput = socket.getInputStream();
        } catch (IOException e) {
            Log.e(TAG, "Error creating input stream", e);
        }

        OutputStream tmpOutput = null;
        try {
            tmpOutput = socket.getOutputStream();
        } catch (IOException e) {
            Log.e(TAG, "Error creating output stream", e);
        }

        this.inputStream = tmpInput;
        this.outputStream = tmpOutput;
        this.start();
    }

    public void disconnect() {
        this.interrupt();
        try {
            this.socket.close();
        } catch (IOException e) {
            Log.e(TAG, "Error closing socket", e);
        }
    }

    @Override
    public void run() {
        while (this.socket.isConnected()) {
            writeData();
        }
    }

    public void pushWriteData(byte[] data) {
        if ((writeSize + data.length) > MAX_WRITE) {
            return;
        }
        int currentIndex = (writeIndex + writeSize) % MAX_WRITE;
        try {
            writeSem.tryAcquire(1, 30, TimeUnit.MICROSECONDS);
            System.arraycopy(data, 0, writeData, currentIndex, data.length);
            writeSize += data.length;
            writeSem.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void writeData() {
        if (writeSize == 0) {
            return;
        }
        try {
            byte[] writeData = new byte[writeSize];
            System.arraycopy(this.writeData, 0, writeData, writeIndex, writeSize);
            writeSem.tryAcquire(1, 30, TimeUnit.MICROSECONDS);
            try {
                this.outputStream.write(writeData);
                writeSize = 0;
                writeIndex = (writeIndex + writeSize) % MAX_WRITE;
            } catch (IOException e) {
                e.printStackTrace();
            }
            writeSem.release();
        } catch (InterruptedException e) {
            Log.e(TAG, "Error acquiring write semaphore", e);
        }
    }

    private void readData() {
        byte[] lData = new byte[1024];
        int nBytes;

        try {
            nBytes = inputStream.read(lData);
            for (int i = 0; i < nBytes; i++) {
                readData.push(lData[i]);
            }
        } catch(java.io.IOException e) {
            Log.e(TAG, "Error reading data", e);
        }
    }
}
